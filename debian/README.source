Integrating original source into local repository
-------------------------------------------------

  (This section is adapted from Debian's QtBase packaging repo. Thanks!)

  Obviously, packaging process requires to have original source next to the
  debian/ directory. Since it is not allowed to ship upstream source in the
  public packaging branches, it needs to be retrieved and managed outside
  packaging branches. This is the way I (Ratchanan) preferred (Qt's first
  choice):

    # (only once) Ignore everything (upstream source) but files under debian/.
    $ git config core.excludesfile debian/upstreamignore

    # Checkout a packaging branch
    $ git checkout ubports/focal # Or whatever branch you'll work on.

    # Remove all untracked files including ignored ones. This cleans up the
    # root directory from old upstream sources (if any)
    $ git clean -xdff

    # Download the tarball according to ubports.source_location, then run
    $ origtargz --unpack

    # Do packaging, committing, finally push...

  If you're curious about what are Qt's other choices, go to Debian's QtBase
  packaging repo [1] to find out.

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>, 10 December 2020.

[1] https://salsa.debian.org/qt-kde-team/qt/qtbase/-/blob/master/debian/README.source
