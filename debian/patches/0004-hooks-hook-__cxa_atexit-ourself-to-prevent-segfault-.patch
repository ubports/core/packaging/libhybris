From 05ec192091a59e3c1cdfc54c56553070b55e02e9 Mon Sep 17 00:00:00 2001
From: Ratchanan Srirattanamet <ratchanan@ubports.com>
Date: Fri, 20 Nov 2020 01:41:55 +0700
Subject: [PATCH] hooks: hook __cxa_atexit ourself to prevent segfault at exit

If an Android library which place a __cxa_atexit() hook is not
dlclose()'ed before libhybris-common is unloaded, the hook will be
called when the program exit. This can cause segfault if the hook
refers to libhybris'es hooked symbol.

Record libraries that placed hooks, and then call them (through
__cxa_finallize) when we're exiting.

As hooks.c is growing, and because this is easier to implement in C++,
I've decided create a subdirectory specifically to contains hooks in the
future. Files in this subdirectory will be compiled with -fvisibility=
hidden to reduce global symbol pollution.

Origin: vendor
Bug: https://github.com/libhybris/libhybris/issues/471
Forwarded: https://github.com/libhybris/libhybris/pull/470
Last-Update: 2020-12-10
---
 hybris/common/Makefile.am      |  12 ++++
 hybris/common/hooks.c          |   7 +--
 hybris/common/hooks/atexit.cpp | 108 +++++++++++++++++++++++++++++++++
 hybris/common/hooks/atexit.h   |  32 ++++++++++
 4 files changed, 155 insertions(+), 4 deletions(-)
 create mode 100644 hybris/common/hooks/atexit.cpp
 create mode 100644 hybris/common/hooks/atexit.h

diff --git a/hybris/common/Makefile.am b/hybris/common/Makefile.am
index 72ebbea..4d8bd19 100644
--- a/hybris/common/Makefile.am
+++ b/hybris/common/Makefile.am
@@ -1,5 +1,7 @@
 lib_LTLIBRARIES = \
 	libhybris-common.la
+noinst_LTLIBRARIES = \
+	hooks/libhybris-hooks.la
 
 SUBDIRS =
 
@@ -49,6 +51,9 @@ libhybris_common_la_SOURCES += \
 	wrapper_code_generic_arm.c
 endif
 
+libhybris_common_la_LIBADD = \
+	hooks/libhybris-hooks.la
+
 libhybris_common_la_CPPFLAGS = \
 	-I$(top_srcdir)/include \
 	$(ANDROID_HEADERS_CFLAGS) \
@@ -101,3 +106,10 @@ libhybris_common_la_LDFLAGS = \
 	-lstdc++ \
 	-lpthread \
 	-version-info "$(LT_CURRENT)":"$(LT_REVISION)":"$(LT_AGE)"
+
+hooks_libhybris_hooks_la_SOURCES = \
+	hooks/atexit.cpp
+
+hooks_libhybris_hooks_la_CXXFLAGS = \
+	$(libhybris_common_la_CXXFLAGS) \
+	-fvisibility=hidden
diff --git a/hybris/common/hooks.c b/hybris/common/hooks.c
index 0df177a..9e0ecbd 100644
--- a/hybris/common/hooks.c
+++ b/hybris/common/hooks.c
@@ -22,6 +22,7 @@
 #include <hybris/common/binding.h>
 
 #include "hooks_shm.h"
+#include "hooks/atexit.h"
 
 #include <stdio.h>
 #include <stdarg.h>
@@ -2188,8 +2189,6 @@ int _hybris_hook_clearenv(void)
     return clearenv();
 }
 
-extern int __cxa_atexit(void (*)(void*), void*, void*);
-extern void __cxa_finalize(void * d);
 extern int __cxa_thread_atexit(void (*dtor)(void *), void *obj,
                                void *dso_symbol);
 
@@ -3051,8 +3050,8 @@ static struct _hook hooks_common[] = {
     /* grp.h */
     HOOK_DIRECT_NO_DEBUG(getgrgid),
     /* C++ ABI */
-    HOOK_DIRECT_NO_DEBUG(__cxa_atexit),
-    HOOK_DIRECT_NO_DEBUG(__cxa_finalize),
+    HOOK_INDIRECT(__cxa_atexit),
+    HOOK_INDIRECT(__cxa_finalize),
     HOOK_INDIRECT(__cxa_thread_atexit),
     /* sys/prctl.h */
     HOOK_INDIRECT(prctl),
diff --git a/hybris/common/hooks/atexit.cpp b/hybris/common/hooks/atexit.cpp
new file mode 100644
index 0000000..b9cd180
--- /dev/null
+++ b/hybris/common/hooks/atexit.cpp
@@ -0,0 +1,108 @@
+/*
+ * Copyright (C) 2020 UBports Foundation
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ *
+ */
+
+/*
+ * The idea of hooking __cxa_atexit is to make sure that atexit hooks from
+ * Android libraries are not called after we're unloaded.
+ *
+ * Imagine the following:
+ *   - A Glibc library is dlopen()'ed, which loads libhybris-common.so
+ *   - That library dlopen() an Android library which registers an atexit hook.
+ *     That atexit hook uses a hooked symbol from libhybris-common.so
+ *   - The Glibc library is dlclose()'ed, however, it doesn't register an
+ *     atexit() hook to dlclose() the Android library, leaving atexit hooks
+ *     inside Glibc's list.
+ *   - As that Glibc library is the only user of libhybris-common.so, it gets
+ *     unloaded too.
+ *   - When the program exits, Glibc will call the Android library's atexit
+ *     hook. However, as libhybris-common.so is already unloaded, the hook will
+ *     segfault when trying to call the hooked function.
+ *
+ * By calling the hooks functions by the time we (libhybris-common.so) are
+ * exiting, we can avoid the segfault, although we still have the lib lying
+ * inside the address space.
+ *
+ * Preferbly, I would like to dlclose() all loaded libraries when we're
+ * unloaded, although I haven't seen a way to do so.
+ */
+
+#include <mutex>
+#include <set>
+
+#include "atexit.h"
+
+extern "C" int __cxa_atexit(void (*)(void*), void*, void*);
+extern "C" void __cxa_finalize(void * d);
+
+class SeenDSOCleanup {
+public:
+    int hook_atexit(void (*func)(void*), void * arg, void * dso_handle)
+    {
+        if (dso_handle == NULL)
+            // Shouldn't happen, but just in case.
+            return __cxa_atexit(func, arg, dso_handle);
+
+        {
+            std::lock_guard<std::mutex> l(seen_dso_handles_mutex);
+            // std::set should do nothing if the dso is seen.
+            seen_dso_handles.insert(dso_handle);
+        }
+
+        return __cxa_atexit(func, arg, dso_handle);
+    }
+
+    void hook_finalize(void * dso_handle)
+    {
+        if (dso_handle == NULL)
+            return;
+
+        {
+            std::lock_guard<std::mutex> l(seen_dso_handles_mutex);
+
+            // erase() returns number of elements removed.
+            if (seen_dso_handles.erase(dso_handle) == 0)
+                // Probably been cleaned by other invocation or didn't register
+                // any hook.
+                return;
+        }
+
+        __cxa_finalize(dso_handle);
+    }
+
+    ~SeenDSOCleanup()
+    {
+        for (auto dso_handle: seen_dso_handles)
+            // Glibc releases a lock before calling this, so it's safe to call
+            // this here.
+            __cxa_finalize(dso_handle);
+    }
+private:
+    std::set<void *> seen_dso_handles;
+    std::mutex seen_dso_handles_mutex;
+};
+
+static class SeenDSOCleanup seen_dso_cleanup;
+
+int _hybris_hook___cxa_atexit(void (*func)(void*), void * arg, void * dso_handle)
+{
+    return seen_dso_cleanup.hook_atexit(func, arg, dso_handle);
+}
+
+void _hybris_hook___cxa_finalize(void * d)
+{
+    return seen_dso_cleanup.hook_finalize(d);
+}
diff --git a/hybris/common/hooks/atexit.h b/hybris/common/hooks/atexit.h
new file mode 100644
index 0000000..beba7b7
--- /dev/null
+++ b/hybris/common/hooks/atexit.h
@@ -0,0 +1,32 @@
+/*
+ * Copyright (C) 2020 UBports Foundation
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ *
+ */
+
+#ifndef _HYBRIS_HOOKS_ATEXIT_H_
+#define _HYBRIS_HOOKS_ATEXIT_H_
+
+#ifdef __cplusplus
+extern "C" {
+#endif
+
+int _hybris_hook___cxa_atexit(void (*func)(void*), void * arg, void * dso_handle);
+void _hybris_hook___cxa_finalize(void * d);
+
+#ifdef __cplusplus
+}
+#endif
+
+#endif
-- 
2.31.1

