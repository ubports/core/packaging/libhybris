From 88e38b07c2c4b36c434cac9296a8fc7f7512e7e3 Mon Sep 17 00:00:00 2001
From: Alfred Neumayer <dev.beidl@gmail.com>
Date: Fri, 5 Apr 2019 00:54:42 +0200
Subject: [PATCH] hooks: hack for the PowerVR crashes on MX4

The MX4 has issues starting unity-system-compositor due to
some blob creating format strings with floats and failing.
This change adds an ugly hack to work around those issues.

The hack has to be removed sometime, but push it now to unblock
edge from shipping more changes related to this issue.

Origin: vendor
Bug-UBports: https://github.com/ubports/ubuntu-touch/issues/1052
Forwarded: not-needed
Last-Update: 2020-12-10
---
 hybris/common/hooks.c | 26 +++++++++++++++++++++++++-
 1 file changed, 25 insertions(+), 1 deletion(-)

diff --git a/hybris/common/hooks.c b/hybris/common/hooks.c
index a35fe44..0df177a 100644
--- a/hybris/common/hooks.c
+++ b/hybris/common/hooks.c
@@ -129,6 +129,8 @@ void *(*_android_get_exported_namespace)(const char* name) = NULL;
 void * (*_android_shared_globals)() = NULL;
 #endif
 
+static int use_vsnprintf_blacklist = 0;
+
 /* TODO:
 *  - Check if the int arguments at attr_set/get match the ones at Android
 *  - Check how to deal with memory leaks (specially with static initializers)
@@ -1472,6 +1474,25 @@ static FILE *_get_actual_fp(FILE *fp)
     return fp;
 }
 
+/*
+ * "Blacklist" certain vsnprintf calls the PowerVR driver uses to
+ * print information to logcat, crashing in the process of doing so.
+ */
+static int _hybris_hook_vsnprintf(char *s, size_t n,
+                                  const char *format, va_list ap)
+{
+    TRACE_HOOK("str %s size %d format '%s'", s, n, format);
+
+    if (use_vsnprintf_blacklist) {
+        if (strncmp("[%s] xdpi        : %f", format, 21) == 0 ||
+            strncmp("[%s] ydpi        : %f", format, 21) == 0) {
+            return 0;
+        }
+    }
+
+    return vsnprintf(s, n, format, ap);
+}
+
 static void _hybris_hook_clearerr(FILE *fp)
 {
     TRACE_HOOK("fp %p", fp);
@@ -2911,7 +2932,7 @@ static struct _hook hooks_common[] = {
     HOOK_DIRECT_NO_DEBUG(vasprintf),
     HOOK_DIRECT_NO_DEBUG(snprintf),
     HOOK_DIRECT_NO_DEBUG(vsprintf),
-    HOOK_DIRECT_NO_DEBUG(vsnprintf),
+    HOOK_INDIRECT(vsnprintf),
     HOOK_INDIRECT(clearerr),
     HOOK_INDIRECT(fclose),
     HOOK_INDIRECT(feof),
@@ -3225,6 +3246,9 @@ static int get_android_sdk_version()
             strcmp(device_name, "cooler") == 0 ||
             strcmp(device_name, "turbo") == 0)
             sdk_version = 19;
+        /* vsnprintf blacklist to work around crashes in the PowerVR blob */
+        if (strcmp(device_name, "arale") == 0)
+            use_vsnprintf_blacklist = 1;
     }
 #endif
 
-- 
2.31.1

